package cat.itb.gestitb_rv;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

class MissedAttendanceViewHolder extends RecyclerView.ViewHolder {

    TextView studentName, date, module, justified;

    public MissedAttendanceViewHolder(@NonNull View itemView) {
        super(itemView);

        studentName = itemView.findViewById(R.id.nameTextView);
        date = itemView.findViewById(R.id.dateTextView);
        module = itemView.findViewById(R.id.moduleTextView);
        justified = itemView.findViewById(R.id.justifiedTextView);
    }

    public void bindData(MissedAttendanceModel missedAttendanceModel){
        studentName.setText(missedAttendanceModel.getName());
        date.setText(missedAttendanceModel.getDate().toString());
        module.setText(missedAttendanceModel.getModule());
        if(missedAttendanceModel.isJustified()){
            justified.setText("Justificada");
        }
        else justified.setText("No justificada");
    }


}

public class MissedAttendanceAdapter extends RecyclerView.Adapter<MissedAttendanceViewHolder> {
    List<MissedAttendanceModel> list;

    public MissedAttendanceAdapter(List<MissedAttendanceModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MissedAttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.missed_attendance_list_item, parent, false);
        return new MissedAttendanceViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull MissedAttendanceViewHolder holder, int position) {
        holder.bindData(list.get(position));
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

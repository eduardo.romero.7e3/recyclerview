package cat.itb.gestitb_rv;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MissedAttendanceViewModel extends ViewModel {
    private List<MissedAttendanceModel> list = new ArrayList<MissedAttendanceModel>();

    public MissedAttendanceViewModel() {
    }

    public void init(){
        final MissedAttendanceModel model = new MissedAttendanceModel("Example Student", "MO0", Calendar.getInstance().getTime(), true);
        for(int i = 0; i < 100; i++){
            list.add(model);
        }
    }

    public List<MissedAttendanceModel> getList(){
        return this.list;
    }

    public void add(MissedAttendanceModel missedAttendanceModel){
        list.add(missedAttendanceModel);
    }

    public void remove(int index){
        list.remove(index);
    }

    public MissedAttendanceModel get(int index){
        return list.get(index);
    }
}

package cat.itb.gestitb_rv;

import java.util.Date;

public class MissedAttendanceModel {
    private String name;
    private String module;
    private Date date;
    private boolean justified;

    public MissedAttendanceModel(String name, String module, Date date, boolean justified) {
        this.name = name;
        this.module = module;
        this.date = date;
        this.justified = justified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isJustified() {
        return justified;
    }

    public void setJustified(boolean justified) {
        this.justified = justified;
    }
}

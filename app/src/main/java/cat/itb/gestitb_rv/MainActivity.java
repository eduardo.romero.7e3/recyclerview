package cat.itb.gestitb_rv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment missedAttendanceList = getSupportFragmentManager().findFragmentById(R.id.fragmentActivityContainer);

        if(missedAttendanceList == null){
            missedAttendanceList = new MissedAttendanceFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragmentActivityContainer, missedAttendanceList).commit();
        }
    }
}